class Logger {
  constructor() {
    this.log = [];
    this.maxLogs = 5;
  }

  addLog(message) {
    if (this.log.length > this.maxLogs - 1) {
      this.log.shift();
    }
    this.log.push(message);
  }

  clearLog() {
    this.log = [];
  }

  getLogs() {
    return this.log;
  }
}